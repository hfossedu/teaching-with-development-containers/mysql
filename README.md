# MySQL

This project provides an example of how to use a Visual Studio Code Dev Container to work with MySQL.

## Features

- MySQL running in a Docker container
- Three different interfaces to work with MySQL
    - [MySQL VS Code extension](https://marketplace.visualstudio.com/items?itemName=cweijan.vscode-mysql-client2) (cweijan.vscode-mysql-client2)
    - `default-mysql-client` command line client installed
    - [Adminer](https://www.adminer.org/) web client running in a Docker container

## Requires

* Docker
* VS Code
* VS Code extension "Remote - Containers"

## Using

### MySQL VS Code Extension

1. Click on the `Database` icon ![](db.png) in the left menubar
2. Click on `Add Connection` (`+` icon) at the top of the left window
3. Enter a connection name in `Name`
4. Enter `example` in `Password`
5. Enter all databases you want displayed in `Showed Databases` (for example `mysql`)
6. Click `+Connect` button

### `default-mysql-client` Command Line Client

1. Open a terminal in VS Code
2. Enter `mysql -h 127.0.0.1 -u root -pexample`

### Adminer Web Client

1. Open a browser to [http://localhost:8088/](http://localhost:8088/)
2. Username: `root`
3. Password: `example`
4. Click `Login`

## Notes

1. This is an example that is meant to show you multiple ways you could use a VS Code Dev Container to work with MySQL.
2. You would likely only use 1 of these interfaces, not all three.
3. There are probably other interfaces you could use.
4. MySQL Docker image is pinned to version 8.
5. Adminer Docker image is pinned to version 4.

---
Copyright &copy; 2022 The HFOSSedu Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.